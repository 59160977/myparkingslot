package buu.informatics.st59160977.slotcars

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import buu.informatics.st59160977.slotcars.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

private lateinit var binding: ActivityMainBinding
    private val myparking1: MyParking = MyParking("","","","No")
    private val myparking2: MyParking = MyParking("","","","No")
    private val myparking3: MyParking = MyParking("","","","No")
    private var number: Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        hidetextbtn()

        binding.apply {
            buttonParking1.setOnClickListener {
                number = 1
                datashow(it)

            }
            buttonParking2.setOnClickListener {
                number = 2
                datashow(it)

            }
            buttonParking3.setOnClickListener {
                number = 3
                datashow(it)

            }
            buttonUpdate.setOnClickListener {
                updateslot(it)

            }
            buttonDelete.setOnClickListener {
                deleteslot(it)

            }
        }
        binding.myparking1 = myparking1
        binding.myparking2 = myparking2
        binding.myparking3 = myparking3
    }

    private fun updateslot(view: View) {
        binding.apply {

            licenseText.visibility = View.GONE
            brandText.visibility = View.GONE
            ownerText.visibility = View.GONE
            buttonDelete.visibility = View.GONE
            buttonUpdate.visibility = View.GONE

            if (number == 1) {
                myparking1?.licenseplate = licenseText.text.toString()
                myparking1?.brand = brandText.text.toString()
                myparking1?.cusname = ownerText.text.toString()
                myparking1?.status = licenseText.text.toString()
                buttonParking1.setBackgroundColor(Color.RED)
            }
            else if (number == 2){
                myparking2?.licenseplate = licenseText.text.toString()
                myparking2?.brand = brandText.text.toString()
                myparking2?.cusname = ownerText.text.toString()
                myparking2?.status = licenseText.text.toString()
                buttonParking2.setBackgroundColor(Color.RED)
            }
            else {
                myparking3?.licenseplate = licenseText.text.toString()
                myparking3?.brand = brandText.text.toString()
                myparking3?.cusname = ownerText.text.toString()
                myparking3?.status = licenseText.text.toString()
                buttonParking3.setBackgroundColor(Color.RED)
            }
            invalidateAll()
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

        }
    }

    private fun deleteslot(view: View) {
        binding.apply {

            licenseText.visibility = View.GONE
            brandText.visibility = View.GONE
            ownerText.visibility = View.GONE
            buttonDelete.visibility = View.GONE
            buttonUpdate.visibility = View.GONE

            if (number == 1) {
                myparking1?.licenseplate = ""
                myparking1?.brand =""
                myparking1?.cusname = ""
                myparking1?.status = "No...."
                buttonParking1.setBackgroundColor(Color.RED)
            }
            else if (number == 2){
                myparking2?.licenseplate = ""
                myparking2?.brand =""
                myparking2?.cusname = ""
                myparking2?.status = "No"
                buttonParking2.setBackgroundColor(Color.RED)
            }
            else {
                myparking3?.licenseplate = ""
                myparking3?.brand =""
                myparking3?.cusname = ""
                myparking3?.status = "No"
                buttonParking3.setBackgroundColor(Color.RED)
            }
            invalidateAll()
        }
    }
    private fun datashow(view: View){
        binding.apply {
            licenseText.visibility = View.VISIBLE
            brandText.visibility = View.VISIBLE
            ownerText.visibility = View.VISIBLE
            buttonDelete.visibility = View.VISIBLE
            buttonUpdate.visibility = View.VISIBLE

            if(number == 1){
                licenseText.setText(myparking1?.licenseplate)
                brandText.setText(myparking1?.brand)
                ownerText.setText(myparking1?.cusname)
            }
            else if (number == 2){
                licenseText.setText(myparking2?.licenseplate)
                brandText.setText(myparking2?.brand)
                ownerText.setText(myparking2?.cusname)
            }
            else{
                licenseText.setText(myparking3?.licenseplate)
                brandText.setText(myparking3?.brand)
                ownerText.setText(myparking3?.cusname)
            }
            invalidateAll()
        }
    }
    private fun settextdata(){
        val slotOne = binding.buttonParking1
        val slotTwo = binding.buttonParking2
        val slotThree = binding.buttonParking3
        val update = binding.buttonUpdate
        val delete = binding.buttonDelete

    }
    private fun hidetextbtn(){
        binding.apply {
            licenseText.visibility = View.GONE
            brandText.visibility = View.GONE
            ownerText.visibility = View.GONE
            buttonUpdate.visibility = View.GONE
            buttonDelete.visibility = View.GONE
        }
    }
}


